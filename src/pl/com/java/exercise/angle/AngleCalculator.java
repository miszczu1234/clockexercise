package pl.com.java.exercise.angle;

public class AngleCalculator {
	private Clock clock;
	
	private static final double MIN_POINTER_ROAD;
	private static final double HOUR_POINTER_ROAD;
	
	static{
		MIN_POINTER_ROAD= 360.0 / 60.0; 
		HOUR_POINTER_ROAD= 360.0 / 720.0;
	}
	
	public AngleCalculator(Clock clock) {
		this.clock= clock;
	}
	
	public Clock getClock() {
		return clock;
	}
	
	private int getMinutesFromStart(){
		int hour= clock.getHours();
		int minutes= (hour == 12) ? 0 : (hour * 60);
		minutes+= clock.getMinutes();
		
		return minutes;
	}
	
	private double getHourPointerAngle(){
		double angleFromStart= ((double)getMinutesFromStart()) * HOUR_POINTER_ROAD;
		
		return angleFromStart;
	}
	
	private double getMinutePointerAngle(){
		double angleFromStart= ((double)clock.getMinutes()) * MIN_POINTER_ROAD;
		
		return angleFromStart;
	}
	
	public double getAngleBetweenPointers(){
		double minutePointerAngle= getMinutePointerAngle();
		double hourPointerAngle= getHourPointerAngle();
		
		return ((minutePointerAngle > hourPointerAngle) ? (minutePointerAngle - hourPointerAngle) 
				: (hourPointerAngle - minutePointerAngle)); 
	}
}
