package pl.com.java.exercise.angle;

import pl.com.java.exercise.angle.exception.InputArgumentsException;

public class InputArgsParser {
	
	private static final int MIN_ARG_INDEX= 1;
	private static final int HOUR_ARG_INDEX= 0;
	
	private String args[];
	private int hour;
	private int min;
	
	private void validateValues(int hour, int min) throws InputArgumentsException{
		if((hour > 12) || (hour < 1)){
			throw new InputArgumentsException("Hour value is incorrect. Hour must be grater or equal 1 and less or equal 12.");
		}
		if((min > 60) || (min < 0)){
			throw new InputArgumentsException("Minute value is incorrect. Minute must be greater or equal 0 and less or equal 59");
		}
	}
	
	private void validInputArgs(String[] args) throws InputArgumentsException{
		if(args.length < 2){
			throw new InputArgumentsException("Wrong input parameters. Please insert clock hour in format : 'hh MM' as input program arguments.");
		}
		try{
			hour= Integer.parseInt(args[HOUR_ARG_INDEX]);
			min= Integer.parseInt(args[MIN_ARG_INDEX]);
			validateValues(hour, min);
		}catch(NumberFormatException e){
			throw new InputArgumentsException("Wrong input parameters. Provided input arguments aren't numbers.");
		}
	}
	
	public InputArgsParser(String args[]) {
		this.args= args;
	}
	
	public String validate(){
		String validMsg= "";
		
		try{
			validInputArgs(args);
		}catch(InputArgumentsException e){
			validMsg= e.getMessage();
		}
		
		return validMsg;
	}

	public String[] getArgs() {
		return args;
	}

	public int getHour() {
		return hour;
	}

	public int getMin() {
		return min;
	}
}
