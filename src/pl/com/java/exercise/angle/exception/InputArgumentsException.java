package pl.com.java.exercise.angle.exception;

public class InputArgumentsException extends Exception{
	
	private static final long serialVersionUID = 1L;

	public InputArgumentsException() {
		super();
	}
	
	public InputArgumentsException(String message){
		super(message);
	}
}
