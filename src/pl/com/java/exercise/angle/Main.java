package pl.com.java.exercise.angle;

public class Main {
	public static void main(String args[]){
		InputArgsParser inputArgsParser= new InputArgsParser(args);
		String validMsg= inputArgsParser.validate();
		if(!validMsg.isEmpty()){
			System.out.println(validMsg);
			return;
		}
		int hour= inputArgsParser.getHour();
		int minutes= inputArgsParser.getMin();
		System.out.println(String.format("Input arguments : hour=%d, minute=%d", inputArgsParser.getHour(), inputArgsParser.getMin()));
		Clock clock= new Clock(minutes, hour);
		AngleCalculator angleCalc= new AngleCalculator(clock);
		System.out.println(String.format("Angle between clock pointers : %f", angleCalc.getAngleBetweenPointers()));
	}
}
