package pl.com.java.exercise.angle;

public class Clock {
	private final int minutes;
	private final int hours;
	
	public Clock(int minutes, int hours) {
		this.minutes= minutes;
		this.hours= hours;
	}
	
	public int getMinutes() {
		return minutes;
	}
	
	public int getHours() {
		return hours;
	}
}
